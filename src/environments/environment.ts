// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  baseUrl : 'https://api.dev.icheck.vn/api/v1',
  production: false,
  firebase: {
    apiKey: "AIzaSyAYf-V6sdmiDOeLPbHzgL7DN9Awzb2gZ6o",
    authDomain: "icheck-dev-d9f70.firebaseapp.com",
    databaseURL: "https://icheck-dev-d9f70.firebaseio.com",
    projectId: "icheck-dev-d9f70",
    storageBucket: "icheck-dev-d9f70.appspot.com",
    messagingSenderId: "738064619121",
    appId: "1:738064619121:web:89cb54bce980de57"
  },
  oidc: {
    authority: 'https://accounts.dev.icheck.vn',
    clientId: '8406fbfa-babd-4ba5-abd1-8209e9d10a1a',
    responseType: 'id_token token',
    scope: 'openid offline'
  },
  apiBaseUrl: 'https://api.dev.icheck.vn/api/v1',
  uploadBaseUrl: 'https://upload.dev.icheck.vn',
  url_icheck_vn: 'https://icheck.vn',
  url_chat : 'https://chatapi.dev.icheck.vn',
  url_image_old : 'https://static.icheck.com.vn',
  qrcodeBaseUrl: 'http://localhost:4200',
  mapUrl: 'https://maps.googleapis.com/maps/api'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
