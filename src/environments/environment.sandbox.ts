export const environment = {
  production: false,
  oidc: {
    authority: 'https://accounts.dev.icheck.vn',
    clientId: '8406fbfa-babd-4ba5-abd1-8209e9d10a1a',
    responseType: 'id_token token',
    scope: 'openid offline'
  },
  apiBaseUrl: 'https://api.dev.icheck.vn/api/v1',
  uploadBaseUrl: 'https://upload.dev.icheck.vn',
  url_icheck_vn: 'https://icheck.vn',
  url_chat : 'https://chatapi.dev.icheck.vn',
  qrcodeBaseUrl: 'http://localhost:4200',
  url_image_old : 'https://static.icheck.com.vn',
  mapUrl: 'https://maps.googleapis.com/maps/api'
};
