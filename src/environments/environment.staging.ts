export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAYf-V6sdmiDOeLPbHzgL7DN9Awzb2gZ6o",
    authDomain: "icheck-dev-d9f70.firebaseapp.com",
    databaseURL: "https://icheck-dev-d9f70.firebaseio.com",
    projectId: "icheck-dev-d9f70",
    storageBucket: "icheck-dev-d9f70.appspot.com",
    messagingSenderId: "738064619121",
    appId: "1:738064619121:web:89cb54bce980de57"
  },
  oidc: {
    authority: 'https://accounts.dev.icheck.vn',
    clientId: '8406fbfa-babd-4ba5-abd1-8209e9d10a1a',
    responseType: 'id_token token',
    scope: 'openid offline'
  },
  apiBaseUrl: 'https://api.dev.icheck.vn/api/v1',
  uploadBaseUrl: 'https://upload.dev.icheck.vn',
  url_icheck_vn: 'https://icheck.vn',
  url_chat : 'https://chatapi.dev.icheck.vn',
  qrcodeBaseUrl: 'https://qrcode-dev.icheck.com.vn',
  url_image_old : 'https://static.icheck.com.vn',
  mapUrl: 'https://maps.googleapis.com/maps/api'
};
