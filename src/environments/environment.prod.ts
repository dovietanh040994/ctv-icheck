export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCe546KUQ0N7-XgmfwwWDMIKXcwvEWBq_o",
    authDomain: "helical-history-126218.firebaseapp.com",
    databaseURL: "https://helical-history-126218.firebaseio.com",
    projectId: "helical-history-126218",
    storageBucket: "helical-history-126218.appspot.com",
    messagingSenderId: "873655301603",
    appId: "1:873655301603:web:242f0003c514167acb5cd6",
    measurementId: "G-SPR1BYV39E"
  },
  oidc: {
    authority: 'https://accounts.icheck.com.vn',
    clientId: '8406fbfa-babd-4ba5-abd1-8209e9d10a1a',
    responseType: 'id_token token',
    scope: 'openid offline'
  },
  apiBaseUrl: 'https://api.icheck.com.vn/api/v1',
  uploadBaseUrl: 'https://upload.icheck.vn',
  url_icheck_vn: 'https://icheck.vn',
  url_chat : 'https://chat.icheck.com.vn',
  qrcodeBaseUrl: 'https://qrcode.icheck.vn:86',
  url_image_old : 'https://static.icheck.com.vn',
  mapUrl: 'https://maps.googleapis.com/maps/api'
};
