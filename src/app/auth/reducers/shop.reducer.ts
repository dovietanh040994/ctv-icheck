import { Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { ShopActions } from 'src/app/auth/actions';
import { Shop } from 'src/app/auth/models';

export interface State extends EntityState<Shop> {
	selectedId: string | null;
}

export const adapter: EntityAdapter<Shop> = createEntityAdapter<Shop>({
	selectId: shop => shop.id,
	sortComparer: false,
})

export const initialState: State = adapter.getInitialState({
	selectedId: null,
});

export function reducer(state = initialState, action: ShopActions.ShopsPageActionsUnion): State {
	switch (action.type) {

		case ShopActions.getShopById.type:
			return { ...state, selectedId: action.id };

		default:
			return state;
	}
}

export const getSelectedId = (state: State) => state.selectedId