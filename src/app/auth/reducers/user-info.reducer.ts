import { Action } from '@ngrx/store';

import { User } from '../models';
import { UserInfoActions } from '../actions';


export interface State {
  userInfo: User;
  isLoading: boolean;
  isLoaded: boolean;
  error: string;
}

export const initialState: State = {
  userInfo: null,
  isLoading: false,
  isLoaded: false,
  error: null
};

export function reducer(
  state = initialState,
  action: UserInfoActions.UserInfoActions
): State {
  switch (action.type) {

    case UserInfoActions.UserInfoActionTypes.GetUserInfoStart: {
      return {
        ...state,
        error: null,
        isLoaded: false,
        isLoading: true
      };
    }

    case UserInfoActions.UserInfoActionTypes.GetUserInfoSuccess: {
      return {
        ...state,
        userInfo: action.payload.userInfo,
        isLoaded: true,
        isLoading: false
      };
    }

    case UserInfoActions.UserInfoActionTypes.GetUserInfoFailure: {
      return {
        ...state,
        error: action.payload.error,
        isLoaded: false,
        isLoading: false
      };
    }

    default:
      return state;

  }
}

export const getUserInfo = (state: State) => state.userInfo;

export const getIsLoading = (state: State) => state.isLoading;

export const getIsLoaded = (state: State) => state.isLoaded;

export const getError = (state: State) => state.error;
