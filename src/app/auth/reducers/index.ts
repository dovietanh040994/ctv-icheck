import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromAuthentication from './authentication.reducer';
import * as fromUserInfo from './user-info.reducer';
import * as fromShop from './shop.reducer';

export interface State {
  authentication: fromAuthentication.State;
  userInfo: fromUserInfo.State;
  shop: fromShop.State;
}

export const reducers: ActionReducerMap<State> = {
  authentication: fromAuthentication.reducer,
  userInfo: fromUserInfo.reducer,
  shop: fromShop.reducer,
};

export const getAuthState = createFeatureSelector<State>('auth');

export const getAuthenticationState = createSelector(
  getAuthState,
  state => state.authentication
);

export const getUserInfoState = createSelector(
  getAuthState,
  state => state.userInfo
);

export const IsAuthenticationStatusChecked = createSelector(
  getAuthenticationState,
  fromAuthentication.getIsAuthenticationStatusChecked
);

export const getAuthenticationData = createSelector(
  getAuthenticationState,
  fromAuthentication.getAuthenticationData
);

export const getAuthenticationError = createSelector(
  getAuthenticationState,
  fromAuthentication.getError
);

export const getUserInfo = createSelector(
  getUserInfoState,
  fromUserInfo.getUserInfo
);

//shop
export const getShopEntitiesState = createSelector(
  getAuthState,
  state => state.shop
);

export const getSelectedShopId = createSelector(
  getShopEntitiesState,
  fromShop.getSelectedId
)

export const {
  selectIds: getShopIds,
  selectEntities: getShopEntities,
  selectAll: getAllShop,
  selectTotal: getTotalShop,
} = fromShop.adapter.getSelectors(getShopEntitiesState);

export const getSelectedShop = createSelector(
  getShopEntities,
  getSelectedShopId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
)