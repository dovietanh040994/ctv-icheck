import { AuthenticationActions } from '../actions';
import { Authentication } from '../models';


export interface State {
  isAuthenticationStatusChecked: boolean;
  authenticationData: Authentication;
  error: string;
}

export const initialState: State = {
  isAuthenticationStatusChecked: false,
  authenticationData: null,
  error: null
};

export function reducer(
  state = initialState,
  action: AuthenticationActions.AuthenticationActions
): State {
  switch (action.type) {

    case AuthenticationActions.AuthenticationActionTypes.CheckAuthenticationStatus:
      return {
        ...state,
        isAuthenticationStatusChecked: false
      };

    case AuthenticationActions.AuthenticationActionTypes.AuthenticationSuccess: {
      return {
        ...state,
        error: null
      };
    }

    case AuthenticationActions.AuthenticationActionTypes.AuthenticationError: {
      return {
        ...state,
        error: action.payload.error
      };
    }

    case AuthenticationActions.AuthenticationActionTypes.UserIsAuthenticated:
      return {
        ...state,
        authenticationData: action.payload.authenticationData,
        isAuthenticationStatusChecked: true
      };

    case AuthenticationActions.AuthenticationActionTypes.UserIsNotAuthenticated:
      return {
        ...state,
        authenticationData: null,
        isAuthenticationStatusChecked: true
      };

    default:
      return state;

  }
}

export const getIsAuthenticationStatusChecked = (state: State) => state.isAuthenticationStatusChecked;

export const getAuthenticationData = (state: State) => state.authenticationData;

export const getError = (state: State) => state.error;
