import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, switchMap, catchError, tap, exhaustMap } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { AuthenticationActions, UserInfoActions } from '../actions';
import { AuthService } from '../services';


@Injectable()
export class AuthenticationEffects {

  @Effect()
  checkAuthenticationStatus$: Observable<Action> = this.actions$.pipe(
    ofType(AuthenticationActions.AuthenticationActionTypes.CheckAuthenticationStatus),
    switchMap(() => this.authService.getAuthenticationData()
      .pipe(
        map(authenticationData => {
          if (authenticationData) {
            return new AuthenticationActions.UserIsAuthenticated({ authenticationData });
          }

          return new AuthenticationActions.UserIsNotAuthenticated();
        }),
      )
    )
  );

  @Effect({ dispatch: false })
  authorize$ = this.actions$.pipe(
    ofType(AuthenticationActions.AuthenticationActionTypes.Authorize),
    map(action => action.payload.redirectUrl),
    tap(redirectUrl => this.authService.authorize({ redirectUrl })),
  );

  @Effect()
  authenticationComplete$: Observable<Action> = this.actions$.pipe(
    ofType(AuthenticationActions.AuthenticationActionTypes.AuthenticationComplete),
    exhaustMap(() => {
      return this.authService.handleSigninRedirectCallback()
        .pipe(
          switchMap(authenticationData => [
            new AuthenticationActions.AuthenticationSuccess({ authenticationData }),
            new AuthenticationActions.RedirectAuthenticatedUser({ redirectUrl: this.authService.redirectUrl })
          ]),
          catchError(error => of(new AuthenticationActions.AuthenticationError({ error: error.message })))
        );
    })
  );

  @Effect()
  authenticationSuccess$: Observable<Action> = this.actions$.pipe(
    ofType(AuthenticationActions.AuthenticationActionTypes.AuthenticationSuccess),
    map(action => action.payload.authenticationData),
    map(authenticationData => new AuthenticationActions.UserIsAuthenticated({ authenticationData }))
  );

  @Effect({ dispatch: false })
  redirectAuthenticatedUser$ = this.actions$.pipe(
    ofType(AuthenticationActions.AuthenticationActionTypes.RedirectAuthenticatedUser),
    map(action => action.payload.redirectUrl),
    tap(redirectUrl => this.router.navigateByUrl(redirectUrl)),
  );

  @Effect()
  userIsAuthenticated$: Observable<Action> = this.actions$.pipe(
    ofType(AuthenticationActions.AuthenticationActionTypes.UserIsAuthenticated),
    map(() => new UserInfoActions.GetUserInfoStart())
  );

  @Effect({ dispatch: false })
  logout$: Observable<Action> = this.actions$.pipe(
    ofType(AuthenticationActions.AuthenticationActionTypes.Logout),
    tap(() => this.authService.signoutRedirect())
  );

  @Effect()
  logoutComplete$: Observable<Action> = this.actions$.pipe(
    ofType(AuthenticationActions.AuthenticationActionTypes.LogoutComplete),
    switchMap(() => {
      return this.authService.handleSignoutRedirectCallback()
        .pipe(
          map(() => new AuthenticationActions.LogoutSuccess()),
          catchError(error => of(new AuthenticationActions.LogoutError({ error: error.error })))
        );
    })
  );

  @Effect({ dispatch: false })
  logoutSuccess$ = this.actions$.pipe(
    ofType(AuthenticationActions.AuthenticationActionTypes.LogoutSuccess),
    tap(() => this.router.navigateByUrl('/')),
  );

  constructor(
    private actions$: Actions<AuthenticationActions.AuthenticationActions>,
    private router: Router,
    private authService: AuthService) {}

}
