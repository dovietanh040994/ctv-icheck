import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { UserInfoActions } from '../actions';
import { AuthService } from '../services';


@Injectable()
export class UserInfoEffects {

  @Effect()
  getUserInfoStart$: Observable<Action> = this.actions$.pipe(
    ofType(UserInfoActions.UserInfoActionTypes.GetUserInfoStart),
    switchMap(() => {
      return this.authService.getUserInfo()
      .pipe(
        map(userInfo => new UserInfoActions.GetUserInfoSuccess({ userInfo })),
        catchError(error => {
          console.log(error);
          return of(new UserInfoActions.GetUserInfoFailure({ error }));
        })
      );
    })
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService) {}

}
