import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { UserInfoEffects } from './user-info.effects';

describe('UserInfoEffects', () => {
  let actions$: Observable<any>;
  let effects: UserInfoEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserInfoEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(UserInfoEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
