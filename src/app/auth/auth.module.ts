import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AuthRoutingModule } from './auth-routing.module';
import * as fromAuth from './reducers';
import { AuthenticationEffects, UserInfoEffects } from './effects';
import { AuthServiceConfig } from './services';
import { TokenInterceptor } from './interceptors';
import {
  SigninSilentCallbackComponent,
  SigninRedirectCallbackComponent,
  SignoutPopupCallbackComponent,
  SignoutRedirectCallbackComponent
} from './pages';

@NgModule({
  declarations: [
    SigninRedirectCallbackComponent,
    SigninSilentCallbackComponent,
    SignoutRedirectCallbackComponent,
    SignoutPopupCallbackComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    StoreModule.forFeature('auth', fromAuth.reducers),
    EffectsModule.forFeature([AuthenticationEffects, UserInfoEffects])
  ]
})
export class AuthModule {
  static forRoot(config: AuthServiceConfig): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        { provide: AuthServiceConfig, useValue: config },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true
        }
      ]
    };
  }
}
