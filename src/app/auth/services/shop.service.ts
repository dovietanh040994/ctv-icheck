import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

// @ts-ignore
import { BaseService } from 'src/app/services/base.service';
import { Shop } from 'src/app/auth/models';

@Injectable({
  providedIn: 'root'
})
export class ShopService extends BaseService {

  listShop(): Observable<any> {
    return this.get<any>(`/users/me/accounts/shops`);
  }

  find(shopId: any): Observable<any> {
    return this.get<any>(`/shops/${shopId}`);
  }

}
