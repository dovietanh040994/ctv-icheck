import { Injectable, Optional } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserManager, Log, User as OidcUser, UserManagerSettings } from 'oidc-client';

import { User, Authentication } from '../models';


export class AuthServiceConfig implements UserManagerSettings { }

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private oidcClient: UserManager;

  constructor(@Optional() config: AuthServiceConfig) {
    Log.logger = console;
    const location = (config as any).locationGetter();

    config['redirect_uri'] = `${location.origin}/auth/callback`;
    config['silent_redirect_uri'] = `${location.origin}/auth/silent-callback`;
    config['post_logout_redirect_uri'] = `${location.origin}/auth/signout-callback`;

    this.oidcClient = new UserManager(config);
  }

  get redirectUrl(): string {
    return localStorage.getItem('redirect_url');
  }

  set redirectUrl(value) {
    if (!value) {
      localStorage.removeItem('redirect_url');
    } else {
      localStorage.setItem('redirect_url', value);
    }
  }

  authorize(options: { redirectUrl: string; }): Observable<any> {
    this.redirectUrl = options.redirectUrl;

    return this.signinRedirect();
  }

  signinRedirect(): Observable<any> {
    return from(this.oidcClient.signinRedirect());
  }

  signinSilent(): Observable<any> {
    return from(this.oidcClient.signinSilent());
  }

  handleSigninRedirectCallback(): Observable<OidcUser> {
    return from(this.oidcClient.signinRedirectCallback());
  }

  handleSigninSilentCallback(): Observable<any> {
    return from(this.oidcClient.signinSilentCallback());
  }

  signoutRedirect(): Observable<any> {
    return from(this.oidcClient.signoutRedirect());
  }

  signoutPopup(): Observable<any> {
    return from(this.oidcClient.signoutPopup());
  }

  handleSignoutRedirectCallback(): Observable<any> {
    return from(this.oidcClient.signoutRedirectCallback());
  }

  handleSignoutPopupCallback(): Observable<any> {
    return from(this.oidcClient.signoutPopupCallback());
  }

  getAuthenticationData(): Observable<Authentication> {
    return from(this.oidcClient.getUser());
  }

  getUserInfo(): Observable<User> {
    return this.getAuthenticationData()
      .pipe(
        map(authenticationData => authenticationData.profile)
      );
  }

  registerEventCallback(name: string, callback: (...ev: any[]) => void): void {
    this.oidcClient.events[name](callback);
  }

}
