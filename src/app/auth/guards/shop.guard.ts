import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { first, filter, take, switchMap } from 'rxjs/operators';

import { Store } from '@ngrx/store';
import * as fromAuth from '../reducers';
import { ShopActions } from '../actions';
import { Shop } from '../models';
import { ShopService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class ShopGuard implements CanActivate {

  constructor(
    private store: Store<fromAuth.State>,
    private router: Router,
    private shop: ShopService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (localStorage.getItem('shop_id')) {
      this.store.dispatch(ShopActions.getShopById({ id: localStorage.getItem('shop_id') }));
      return of(true);
    } else {
      return this.shop.listShop()
        .pipe(
          switchMap(res => {
            const shops = res.rows;

            //=============điều hướng=============//
            // block điều hướng block (done)
            // không có cửa hàng điều hướng shop/create (done) 
            // đăng ký thành công điều hướng info (done)
            // trạng thái chờ duyệt điều hướng info (done)
            // có cửa hàng điều hướng dashboard (done)
            // có cửa hàng không cho điều hướng đến shop/create (done)
            // đăng nhập thành công bật tab mới thì vào luôn dashboard không cần đăng nhập lại (won't do)
            //==============end điều hướng=========//

            if (shops.length > 0) {
              let shopId;
              shops.some(s => {
                if (s.status === 2) {
                  this.router.navigate([`/shop/block/${s.id}`]);
                  return of(false);
                } else if (s.status === 0) {
                  this.router.navigate([`/shop/info/${s.id}`]);
                  return of(false);
                } else {
                  shopId = s.id;
                  return true;
                }
              })
              this.store.dispatch(ShopActions.getShopById({ id: shopId }));
              return of(true);
            } else {
              this.router.navigate([`/shop/success`]);
              return of(false);
            }
          })
        );
    }

  };
}
