import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { first, filter, take, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import * as fromAuth from '../reducers';
import { AuthenticationActions } from '../actions';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private store: Store<fromAuth.State>) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.select(fromAuth.IsAuthenticationStatusChecked)
      .pipe(
        filter(statusChecked => statusChecked),
        take(1),
        switchMap(() => {
          return this.store.select(fromAuth.getAuthenticationData)
            .pipe(
              first(),
              switchMap(authenticationData => {
                if (authenticationData && !authenticationData.expired) {
                  return of(true);
                } else {
                  this.store.dispatch(
                    new AuthenticationActions.Authorize({ redirectUrl: state.url })
                  );
                  return of(false);
                }
              })
            );
      }));
  }

}
