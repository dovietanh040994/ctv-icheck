import { TestBed, async, inject } from '@angular/core/testing';

import { ShopBlockRedirectGuard } from './shop-block-redirect.guard';

describe('ShopBlockRedirectGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShopBlockRedirectGuard]
    });
  });

  it('should ...', inject([ShopBlockRedirectGuard], (guard: ShopBlockRedirectGuard) => {
    expect(guard).toBeTruthy();
  }));
});
