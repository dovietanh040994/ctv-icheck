import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ShopService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class ShopBlockRedirectGuard implements CanActivate {

  constructor(
    private router: Router,
    private shop: ShopService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.shop.listShop()
      .pipe(
        switchMap(res => {
          const shops = res.rows;
          if (shops.length > 0) {
            shops.some(s => {
              if (s.status === 1) {
                this.router.navigate([`/`]);
                return of(false);
              }
            })
          }
          return of(true);
        })
      );

  };
}