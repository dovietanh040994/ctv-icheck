import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AuthenticationActions } from '../actions';
import * as fromAuth from '../reducers';

@Component({
  selector: 'con-signout-redirect-callback',
  template: 'Loading ...'
})
export class SignoutRedirectCallbackComponent implements OnInit {

  constructor(private store: Store<fromAuth.State>) { }

  ngOnInit() {
    this.store.dispatch(new AuthenticationActions.LogoutComplete());
  }

}
