import { Component, OnInit } from '@angular/core';

import { AuthService } from '../services';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'con-signin-silent-callback',
  template: 'Loading ...'
})
export class SigninSilentCallbackComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.handleSigninSilentCallback().subscribe(
      () => {},
      (err) => {
        console.log('aaaaaaaa', err);
      }
    );
  }

}
