import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AuthenticationActions } from '../actions';
import * as fromAuth from '../reducers';

@Component({
  selector: 'con-signin-redirect-callback',
  template: `
    <div *ngIf="error$ | async as error; else loading">
      {{error}}
    </div>
    <ng-template #loading>Loading</ng-template>
  `
})
export class SigninRedirectCallbackComponent implements OnInit {

  error$ = this.store.select(fromAuth.getAuthenticationError);

  constructor(private store: Store<fromAuth.State>) { }

  ngOnInit() {
    localStorage.removeItem('shop_id');
    this.store.dispatch(new AuthenticationActions.AuthenticationComplete());
  }

}
