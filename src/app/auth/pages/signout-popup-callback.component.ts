import { Component, OnInit } from '@angular/core';

import { AuthService } from '../services';

@Component({
  selector: 'con-signout-popup-callback',
  template: 'Loading ...'
})
export class SignoutPopupCallbackComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.handleSignoutPopupCallback();
  }

}
