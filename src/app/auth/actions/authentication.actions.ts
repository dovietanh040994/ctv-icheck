import { Action } from '@ngrx/store';

import { Authentication } from '../models';


export enum AuthenticationActionTypes {
  CheckAuthenticationStatus = '[Auth Authentication] Check Authentication Status',
  Authorize = '[Auth Authentication] Authorize',
  AuthenticationComplete = '[Auth Authentication] Authentication Complete',
  SilentAuthenticationComplete = '[Auth Authentication] Silent Authentication Complete',
  AuthenticationSuccess = '[Auth Authentication] Authentication Success',
  AuthenticationError = '[Auth Authentication] Authentication Error',
  RedirectAuthenticatedUser = '[Auth Authentication] Redirect Authenticated User',
  UserIsAuthenticated = '[Auth Authentication] User Is Authenticated',
  UserIsNotAuthenticated = '[Auth Authentication] User Is Not Authenticated',
  Logout = '[Auth Authentication] Logout',
  LogoutComplete = '[Auth Authentication] Logout Complete',
  LogoutSuccess = '[Auth Authentication] Logout Success',
  LogoutError = '[Auth Authentication] Logout Error'
}

export class CheckAuthenticationStatus implements Action {
  readonly type = AuthenticationActionTypes.CheckAuthenticationStatus;
}

export class Authorize implements Action {
  readonly type = AuthenticationActionTypes.Authorize;

  constructor(public payload: { redirectUrl: string }) {}
}

export class AuthenticationComplete implements Action {
  readonly type = AuthenticationActionTypes.AuthenticationComplete;
}

export class UserIsAuthenticated implements Action {
  readonly type = AuthenticationActionTypes.UserIsAuthenticated;

  constructor(public payload: { authenticationData: Authentication }) {}
}

export class UserIsNotAuthenticated implements Action {
  readonly type = AuthenticationActionTypes.UserIsNotAuthenticated;
}

export class AuthenticationSuccess implements Action {
  readonly type = AuthenticationActionTypes.AuthenticationSuccess;

  constructor(public payload: { authenticationData: Authentication }) {}
}

export class AuthenticationError implements Action {
  readonly type = AuthenticationActionTypes.AuthenticationError;

  constructor(public payload: { error: string }) {}
}

export class RedirectAuthenticatedUser implements Action {
  readonly type = AuthenticationActionTypes.RedirectAuthenticatedUser;

  constructor(public payload: { redirectUrl: string }) {}
}

export class Logout implements Action {
  readonly type = AuthenticationActionTypes.Logout;
}

export class LogoutComplete implements Action {
  readonly type = AuthenticationActionTypes.LogoutComplete;
}

export class LogoutSuccess implements Action {
  readonly type = AuthenticationActionTypes.LogoutSuccess;
}

export class LogoutError implements Action {
  readonly type = AuthenticationActionTypes.LogoutError;

  constructor(public payload: { error: string }) {}
}

export type AuthenticationActions =
  | CheckAuthenticationStatus
  | Authorize
  | AuthenticationComplete
  | AuthenticationSuccess
  | AuthenticationError
  | RedirectAuthenticatedUser
  | UserIsAuthenticated
  | UserIsNotAuthenticated
  | Logout
  | LogoutComplete
  | LogoutSuccess
  | LogoutError;
