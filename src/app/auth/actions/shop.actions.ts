import { createAction, props, union } from '@ngrx/store';
import { Shop } from 'src/app/auth/models';

export const getShopById = createAction(
  '[Seller/Shop] Get Shop By Id',
  props<{ id: string }>()
);

const all = union({
	getShopById,
});

export type ShopsPageActionsUnion = typeof all;
