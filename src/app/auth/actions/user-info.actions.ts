import { Action } from '@ngrx/store';

import { User } from '../models';


export enum UserInfoActionTypes {
  GetUserInfoStart = '[Auth User Info] Get User Info Start',
  GetUserInfoSuccess = '[Auth User Info] Get User Info Success',
  GetUserInfoFailure = '[Auth User Info] Get User Info Error'
}

export class GetUserInfoStart implements Action {
  readonly type = UserInfoActionTypes.GetUserInfoStart;
}

export class GetUserInfoSuccess implements Action {
  readonly type = UserInfoActionTypes.GetUserInfoSuccess;

  constructor(public payload: { userInfo: User }) {}
}

export class GetUserInfoFailure implements Action {
  readonly type = UserInfoActionTypes.GetUserInfoFailure;

  constructor(public payload: { error: string }) {}
}

export type UserInfoActions =
  | GetUserInfoStart
  | GetUserInfoSuccess
  | GetUserInfoFailure;
