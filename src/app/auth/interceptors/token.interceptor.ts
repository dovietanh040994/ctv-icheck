import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AuthService } from '../services';
import { mergeMap, map, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  headerName: string;
  authScheme: string;

  constructor(private auth: AuthService) {
    this.headerName = 'Authorization';
    this.authScheme = 'Bearer ';
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.getAccessToken().pipe(
      mergeMap(token => {
        const re = /https?:\/\/(.*?)\.icheck.(com\.vn|vn)/g;
        if (token && re.test(req.url) && !req.url.includes(environment.url_chat)) {
          req = req.clone({
            setHeaders: {
              [this.headerName]: `${this.authScheme}${token}`
            }
          });
        }

        return next.handle(req);
      }),
    );
  }

  protected getAccessToken() {
    return this.auth.getAuthenticationData().pipe(map(data => data.profile.icheck_token.access_token));
  }

  // protected getAccessToken() {
  //   this.auth.getAuthenticationData().pipe(tap(console.log)).subscribe();
  //   return of('c918f3a43fe0f93e9195740c2701741432348c24');
  // }

}
