import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { NotFoundPageComponent } from 'src/app/shared/not-found-page/not-found-page.component'

import {
  SigninRedirectCallbackComponent,
  SigninSilentCallbackComponent,
  SignoutRedirectCallbackComponent
} from './pages';

const routes: Routes = [
  {
    path: 'auth',
    children: [
      {
        path: 'callback',
        component: SigninRedirectCallbackComponent
      },
      {
        path: 'silent-callback',
        component: SigninSilentCallbackComponent
      },
      {
        path: 'signout-callback',
        component: SignoutRedirectCallbackComponent
      }
    ]
  },
  // {
  //   path: '**',
  //   component: NotFoundPageComponent,
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
