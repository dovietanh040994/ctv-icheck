import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  private baseUrl = environment.uploadBaseUrl;

  constructor(private http: HttpClient) { }

  public create(size: number, metadata: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/upload`, { size, metadata });
  }

  public getFile(id: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/files/${id}`, { params: { fields: ['source', 'metadata'] } });
  }

}
